package excel.upload.demo.controller;


import excel.upload.demo.service.UploadServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/upload")
public class UploadController {

    private UploadServices uploadServices;

    @Autowired
    public void setUploadServices(UploadServices uploadServices) {
        this.uploadServices = uploadServices;
    }


    @PostMapping
    public List<Map<String, String>> upload(@RequestParam("file") MultipartFile file) throws Exception {
        try {
            uploadServices.upload(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
