package excel.upload.demo.service;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class UploadServices {

    public List<Map<String, String>> upload(MultipartFile file) throws Exception {
        //1. Create workbook instance from excel sheet
        Path tempDir = Files.createTempDirectory("");
        File tempFile = tempDir.resolve(file.getOriginalFilename()).toFile();
        System.out.println("filename: " + tempFile);
        System.out.println("file extension: " + FilenameUtils.getExtension(tempFile.toString()));
        file.transferTo(tempFile);

        //2. Get to the desired sheet
        Workbook workbook = WorkbookFactory.create(tempFile);
        Sheet sheet = workbook.getSheetAt(0);
        int nRowEndIndex = sheet.getLastRowNum();
        System.out.println("NR Index: " + nRowEndIndex);

        //3. Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        int index = 0;
        while (index <= sheet.getLastRowNum()) {
            Row row = sheet.getRow(index);
            System.out.println("a" + row);
            index++;
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        System.out.print(cell.getNumericCellValue() + "t");
                        break;
                    case Cell.CELL_TYPE_STRING:
                        System.out.print(cell.getStringCellValue() + "t");
                        break;
                }
            }
        }
        return null;
    }
}
